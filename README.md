# 自用说明
目标版本代码已进行审查，基本确认没有后门。

主要删除默认对域名的屏蔽，以及自带黑名单。

使用方式：

- 安装git,curl
- 克隆仓库到本地
- 执行安装命令

如在debian系的linux上进行安装：
```
sudo apt install git curl
git clone https://hyboxu@bitbucket.org/hyboxu/v2ray.git
local_install=true
. install.sh
```

其余步骤参考官方教程README_ORIGIN.md。
